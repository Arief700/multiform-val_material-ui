import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import IntlTelInput from "react-intl-tel-input";
import useForm from "./useForm";
import {
  Typography,
  Button,
  Grid,
  CheckBox,
  TextField,
  OutlinedInput,
  FormControl,
  InputLabel,
  InputAdornment,
  IconButton,
} from "@material-ui/core";

import {
  Visibility,
  VisibilityOff,
  SendSharp,
  BlockSharp,
} from "@material-ui/icons";

import "react-intl-tel-input/dist/main.css";

const useStyles = makeStyles({
  mainContainer: {
    display: "grid",
    justifyContent: "center",
    position: "relative",
    zIndex: 5,
  },
  formContainer: {
    position: "relative",
    width: "28.125rem",
    height: "auto",
    padding: "2rem",
  },
  inputField: {
    width: "100%",
    marginBottom: "1rem",
  },
  btn: {
    width: "100%",
    height: "3rem",
    background: "red",
    color: "#fff",
    "&:hover": {
      background: "red",
      opacity: ".7s",
      transition: ".3 ease-in-out",
    },
  },
  disabledBtn: {
    background: "rgba(0,0,0,0.38)",
    width: "100%",
    height: "3rem",
  },
});

const StepOne = ({ activeStep, steps, handleNext }) => {
  // DEFINE THE STATE SCHEMA
  const stateSchema = {
    firstname: { value: "", error: "" },
    lastname: { value: "", error: "" },
    email: { value: "", error: "" },
    password: { value: "", error: "" },
    confirmPassword: { value: "", error: "" },
  };

  const stateValidatorSchema = {
    firstname: {
      required: true,
      validator: {
        func: (value) =>
          /^([A-Za-z][A-Za-z'-])+([A-Za-z][A-Za-z'-]+)*/.test(value),
        error: "First Name must be more then 1 character",
      },
    },
    lastname: {
      required: true,
      validator: {
        func: (value) =>
          /^([A-Za-z][A-Za-z'-])+([A-Za-z][A-Za-z'-])+([A-Za-z][A-Za-z'-])*/.test(
            value
          ),
        error: "Last Name must be more then 3 character",
      },
    },
    email: {
      required: true,
      validator: {
        func: (value) =>
          /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(
            value
          ),
        error: "Invalid email format",
      },
    },
    password: {
      required: true,
      validator: {
        func: (value) =>
          /^(?=.*[A-Za-z])(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/.test(
            //number 6 mininum dimasukan
            value
          ),
        error:
          "Minimum 6 characters and at least one special characters '@,$,!,%,*,#,?,&'",
      },
    },
  };

  const { values, errors, dirty, handleOnChange } = useForm(
    stateSchema,
    stateValidatorSchema
  );

  const [showPasswordValue, setShowPasswordValue] = useState({
    showPassword: false,
  });
  const [showConfirmPasswordValue, setShowConfirmPasswordValue] = useState({
    showConfirmPassword: false,
  });

  const handleClickShowPassword = () => {
    setShowPasswordValue({
      showPassword: !showPasswordValue.showPassword,
    });
  };
  const handleClickShowConfirmPassword = () => {
    setShowConfirmPasswordValue({
      showConfirmPassword: !showConfirmPasswordValue.showConfirmPassword,
    });
  };

  const { firstname, lastname, email, password, confirmPassword } = values;

  const classes = useStyles();
  return (
    <div className={classes.mainContainer}>
      <Typography variant="h5" style={{ color: "#999", textAlign: "center" }}>
        Sign Up With Email
      </Typography>
      <div className={classes.formContainer}>
        <form autoComplete="off">
          <TextField
            className={classes.inputField}
            label="First Name"
            variant="outlined"
            name="firstname"
            value={firstname}
            onChange={handleOnChange}
          />
          {errors.firstname && dirty.firstname && (
            <Typography
              style={{ marginTop: "0", color: "red", fontWeight: "200" }}
            >
              {errors.firstname}
            </Typography>
          )}
          <TextField
            className={classes.inputField}
            label="Last Name"
            variant="outlined"
            name="lastname"
            value={lastname}
            onChange={handleOnChange}
          />
          {errors.lastname && dirty.lastname && (
            <Typography
              style={{ marginTop: "0", color: "red", fontWeight: "200" }}
            >
              {errors.lastname}
            </Typography>
          )}
          <IntlTelInput preferredCountries={["id"]} />
          <TextField
            className={classes.inputField}
            label="Email"
            variant="outlined"
            name="email"
            value={email}
            onChange={handleOnChange}
          />
          {errors.email && dirty.email && (
            <Typography
              style={{ marginTop: "0", color: "red", fontWeight: "200" }}
            >
              {errors.email}
            </Typography>
          )}
          <FormControl className={classes.inputField} variant="outlined">
            <InputLabel>Password</InputLabel>
            <OutlinedInput
              labelWidth={70}
              type={showPasswordValue.showPassword ? "text" : "password"}
              name="password"
              value={password}
              onChange={handleOnChange}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton edge="end" onClick={handleClickShowPassword}>
                    {showPasswordValue.showPassword ? (
                      <Visibility />
                    ) : (
                      <VisibilityOff />
                    )}
                  </IconButton>
                </InputAdornment>
              }
            />
            {errors.password && dirty.password && (
              <Typography
                style={{ marginTop: "0", color: "red", fontWeight: "200" }}
              >
                {errors.password}
              </Typography>
            )}
          </FormControl>
          <FormControl className={classes.inputField} variant="outlined">
            <InputLabel>Confirm Password</InputLabel>
            <OutlinedInput
              labelWidth={135}
              name="confirmPassword"
              value={confirmPassword}
              onChange={handleOnChange}
              type={
                showConfirmPasswordValue.showConfirmPassword
                  ? "text"
                  : "password"
              }
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    edge="end"
                    onClick={handleClickShowConfirmPassword}
                  >
                    {showConfirmPasswordValue.showConfirmPassword ? (
                      <Visibility />
                    ) : (
                      <VisibilityOff />
                    )}
                  </IconButton>
                </InputAdornment>
              }
            />
            {confirmPassword !== password ? (
              <Typography style={{ color: "red" }}>
                Password do not match
              </Typography>
            ) : null}
          </FormControl>
          {!firstname ||
          !lastname ||
          !email ||
          !password ||
          !confirmPassword ||
          confirmPassword !== password ? (
            <Button
              className={classes.disabledBtn}
              disabled
              variant="contained"
              type="submit"
              endIcon={<BlockSharp />}
            >
              {activeStep === steps.length ? "Finish" : "SIGN UP"}
            </Button>
          ) : (
            <Button
              className={classes.btn}
              variant="contained"
              type="submit"
              onClick={handleNext}
              endIcon={<SendSharp />}
            >
              {activeStep === steps.length ? "Finish" : "SIGN UP"}
            </Button>
          )}
        </form>
      </div>
    </div>
  );
};

export default StepOne;
